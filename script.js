const canvas = document.getElementById("canvas")
canvas.width = window.innerWidth
canvas.height = window.innerHeight

const ctx = canvas.getContext("2d")

const MAX_LINK_LEN = 200,
    DOUBLE_MAX_LINK_LEN = MAX_LINK_LEN * 2,
    MIN_R = 12,
    MAX_R = 20,
    SPEED = 20

class Game {
    circles = []
    links = []
    n = (canvas.width + canvas.height) / 20 | 0

    generateCircles() {
        for (let i = 0; i < this.n; i++) {
            this.circles[i] = new Circle()
        }
    }

    clearScreen() {
        ctx.fillStyle = "rgb(220, 220, 220)"
        ctx.fillRect(0, 0, canvas.width, canvas.height)
    }

    calcLinks() {
        for (let i = 0; i < this.n; i++) {
            for (let k = i + 1; k < this.n; k++) {
                if (Math.abs(this.circles[i].x - this.circles[k].x) > MAX_LINK_LEN || Math.abs(this.circles[i].y - this.circles[k].y) > MAX_LINK_LEN)
                    continue

                const dist = this.circles[i].distanceTo(this.circles[k])
                if (dist <= MAX_LINK_LEN)
                    this.links[this.links.length] = new Link(this.circles[i], this.circles[k], dist)
            }
        }
    }

    drawLinks() {
        for (let i = 0; i < this.links.length; i++) {
            this.links[i].render()
        }
    }

    drawCircles() {
        for (let i = 0; i < this.circles.length; i++) {
            this.circles[i].render()
            this.circles[i].y -= this.circles[i].v
            if (this.circles[i].y < -1 * MAX_LINK_LEN) {
                this.circles[i].setRandomX()
                this.circles[i].setY(canvas.height + MAX_LINK_LEN)
                this.circles[i].setRandomColor()
            }
        }
    }
}

class Link {
    constructor(circle1, circle2, distance) {
        this.circle1 = circle1
        this.circle2 = circle2
        this.distance = distance
    }

    render() {
        const opacity = 1 - (this.distance / (MAX_LINK_LEN / 100)) * 0.01
        ctx.beginPath()
        ctx.strokeStyle = `rgba(0, 0, 0, ${opacity})`
        ctx.moveTo(this.circle1.x, this.circle1.y)
        ctx.lineTo(this.circle2.x, this.circle2.y)
        ctx.stroke()
    }
}

class Circle {
    constructor() {
        this.setRandomX()
        this.setRandomY()

        this.setRandomRadius()
        this.setRandomColor()

        this.v = this.r / SPEED
    }

    setRandomX() {
        this.x = Math.random() * canvas.width | 0
    }

    setRandomY() {
        this.y = Math.random() * (canvas.height + DOUBLE_MAX_LINK_LEN) - MAX_LINK_LEN | 0
    }

    setY(y) {
        this.y = y
    }

    setRandomRadius() {
        this.r = Math.random() * (MAX_R - MIN_R) + MIN_R | 0
    }

    setRandomColor() {
        this.color = `rgba(${(Math.random() * (255 - 12 + 1)) + 10 | 0} , ${(Math.random() * (255 - 12 + 1)) + 10 | 0} , ${(Math.random() * (255 - 12 + 1)) + 10 | 0}, 0.6)`
    }

    distanceTo(circle) {
        return Math.hypot((this.x - circle.x), (this.y - circle.y))
    }

    render() {
        ctx.beginPath()
        ctx.fillStyle = this.color;
        ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI, true)
        ctx.fill()

        ctx.beginPath()
        ctx.fillStyle = "rgba(100, 100, 100, 1)"
        ctx.arc(this.x, this.y, this.r / 2, 0, 2 * Math.PI, true)
        ctx.fill()
    }
}

// Start game
const game = new Game()
game.generateCircles()

function loop() {
    game.clearScreen()
    game.calcLinks()
    game.drawLinks()
    game.drawCircles()
    // TODO: Find better way to clean array
    // game.links.splice(0, game.links.length)
    game.links = []
}

function gameLoop() {
    loop()
    window.requestAnimationFrame(gameLoop)
}

gameLoop()